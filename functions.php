<?php

add_theme_support( 'title-tag' );

add_action('template_redirect', 'bearsmith_disable_author_page');

function bearsmith_disable_author_page() {
    global $wp_query;

    if ( is_author() ) {
        wp_redirect(get_option('home'), 301); 
        exit; 
    }
}